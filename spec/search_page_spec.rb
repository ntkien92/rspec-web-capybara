require_relative '../init'

RSpec.describe 'Search Page', type: :feature do
  describe "the signin process" do
    3000.times do
      it "signs me in" do
        links = [
          'https://shopee.vn/M%E1%BA%B7t-n%E1%BA%A1-d%C6%B0%E1%BB%A1ng-m%C3%B4i-CHOOSY-Nh%E1%BA%ADt-B%E1%BA%A3n-i.62894479.1048933915',
          'https://shopee.vn/Son-M%C3%B4i-Opera-Tint-Oil-Rouge-Lip-Tint-i.62894479.1035153834',
          'https://shopee.vn/VI%C3%8AN-U%E1%BB%90NG-B%E1%BB%94-SUNG-%C4%90A-VITAMIN-DHC-MULTI-VITAMIN-i.62894479.1230835176',
          'https://shopee.vn/Kem-Ch%E1%BB%91ng-N%E1%BA%AFng-Shiseido-Ultimate-Sun-Protection-Lotion-SPF-50-WetForce-i.62894479.1230810519',
          'https://shopee.vn/Vi%C3%AAn-u%E1%BB%91ng-t%E1%BA%A3o-vi%C3%AAn-Spirulina-SGF-c%E1%BB%A7a-Nh%E1%BA%ADt-B%E1%BA%A3n-i.62894479.1155614805',
          'https://shopee.vn/S%E1%BB%AFa-b%C3%A0-b%E1%BA%A7u-Morinaga-i.62894479.1091489125',
          'https://shopee.vn/M%E1%BA%B7t-n%E1%BA%A1-th%C6%B0-gi%E1%BA%A3n-m%E1%BA%AFt-c%E1%BB%A7a-Kao-i.62894479.1091475403',
          'https://shopee.vn/Kem-d%C6%B0%E1%BB%A1ng-%E1%BA%A9m-h%E1%BA%A1t-%C3%BD-d%C4%A9-Naturie-c%E1%BB%A7a-Nh%E1%BA%ADt-i.62894479.1091450335',
          'https://shopee.vn/M%E1%BA%B7t-n%E1%BA%A1-LULULUN-g%C3%B3i-7-mi%E1%BA%BFng-Xanh-Tr%E1%BA%AFng-i.62894479.1050812233',
          # 'https://shopee.vn/Kem-ch%E1%BB%91ng-n%E1%BA%AFng-Shiseido-Mineral-Water-Senka-SPF-50-PA-40ml-i.62894479.1242928442',
          'https://shopee.vn/M%E1%BA%B7t-n%E1%BA%A1-d%C6%B0%E1%BB%A1ng-m%C3%B4i-CHOOSY-Nh%E1%BA%ADt-B%E1%BA%A3n-V%E1%BB%8B-chocolate-s%C3%B4-c%C3%B4-la-i.62894479.1250802208',
          'https://shopee.vn/Ph%E1%BA%A5n-r%C3%B4m-(Ph%E1%BA%A5n-ph%E1%BB%A7)-Shiseido-Baby-Powder-i.62894479.1250789249',
        ]
        # visit 'https://shopee.vn/Ch%C3%A2n-v%C3%A1y-b%C3%BAt-ch%C3%AC-x%E1%BA%BB-sau-h%E1%BB%93ng-t%C3%ADm-n%E1%BB%AF-t%C3%ADnh-(-h%C3%A0ng-may-%C4%91o)-i.54812099.955746141'
        # visit links[rand(links.size)]
        visit links[-1]
        expect(page).to have_content 'shopee'
      end
    end
  end
end
